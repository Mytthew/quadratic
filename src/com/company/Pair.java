package com.company;

public class Pair<T,W> {
	private T first;
	private W second;

	public Pair(T first, W second) {
		this.first = first;
		this.second = second;
	}

	public T getFirst() {
		return this.first;
	}

	public W getSecond() {
		return this.second;
	}
}
