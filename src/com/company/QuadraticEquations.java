package com.company;


public class QuadraticEquations {

    public static void main(String[] args) {
		Solver s1 = new Solver(-2.0, 3.0, -1.0);
		Pair<Printable, Printable> p1 = s1.solve();
		p1.getFirst().print();
		p1.getSecond().print();

		Solver s2 = new Solver(1.0, 3.0, 4.0);
		Pair<Printable, Printable> p2 = s2.solve();
		p2.getFirst().print();
		p2.getSecond().print();

		Solver s3 = new Solver(4.0, 4.0, 1.0);
		Pair<Printable, Printable> p3 = s3.solve();
		p3.getFirst().print();
		p3.getSecond().print();
    }
}
