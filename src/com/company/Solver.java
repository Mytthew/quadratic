package com.company;

import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;

public class Solver {
	private double a;
	private double b;
	private double c;

	public Solver(double a, double b, double c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}

	public Pair<Printable, Printable> solve() {
		double y = Math.pow(this.b, 2) - (4 * this.a * this.c);
		if (y > 0) {
			double x1 = (-this.b - Math.sqrt(y)) / 2 * this.a;
			double x2 = (-this.b + Math.sqrt(y)) / 2 * this.a;
			return new Pair<Printable, Printable>(new Printable() {
				@Override
				public void print() {
					System.out.println(x1);
				}
			}, new Printable() {
				@Override
				public void print() {
					System.out.println(x2);
				}
			});
		} else if (y == 0) {
			double x1 = (-this.b) / 2 * this.a;
			return new Pair<Printable, Printable>(new Printable() {
				@Override
				public void print() {
					System.out.println(x1);
				}
			}, new Printable() {
				@Override
				public void print() {
					System.out.println("Drugie takie samo rozwiązanie rzeczywiste " + x1);
				}
			});
		} else {
			return new Pair<Printable, Printable>(new Printable() {
				@Override
				public void print() {
					System.out.println("Brak rozwiązań rzeczywistych");
				}
			}, new Printable() {
				@Override
				public void print() {
					System.out.println("Brak rozwiązań rzeczywistych");
				}
			});
		}
	}
}
